package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class CreateLeadPage extends ProjectMethods{

	public CreateLeadPage() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how=How.ID,using="createLeadForm_companyName") WebElement eleCompanyName;
	@FindBy(how=How.ID,using="createLeadForm_firstName") WebElement eleFirstName;
	@FindBy(how=How.ID,using="createLeadForm_lastName") WebElement eleLastName;
	@FindBy(how=How.NAME,using="submitButton") WebElement eleCreateLead;
	public ViewLeadPage clickCreateLead()
	{
		click(eleCreateLead);
		return new ViewLeadPage();
		
	}
	
	public CreateLeadPage enterCompanyName(String companyName)
	{
		clearAndType(eleCompanyName, companyName);
		return this;
	}
	public CreateLeadPage enterFirstName(String fName)
	{
		clearAndType(eleFirstName, fName);
		return this;
	}
	public CreateLeadPage enterLastName(String lName)
	{
		clearAndType(eleLastName, lName);
		return this;
	}
	
	
}













