package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class MyLeadsPage extends ProjectMethods{

	public MyLeadsPage() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how=How.XPATH,using="//a[text()='Create Lead']") WebElement eleCreateLeads;
	public CreateLeadPage clickLeads()
	{
		click(eleCreateLeads);
		return new CreateLeadPage(); 
	}
	@FindBy(how=How.LINK_TEXT,using="Find Leads") WebElement eleFindLeads;
	public FindLeadsPage findLeads()
	{
		click(eleFindLeads);
		return new FindLeadsPage();
		
	}
}













