package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class EditLeadPage extends ProjectMethods{

	public EditLeadPage() {
       PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH,using="(//input[@name='companyName'])[2]") WebElement eleCompanyName;
	@FindBy(how=How.XPATH,using="//input[@value='Update']") WebElement eleUpdate;
	
	public EditLeadPage enterCompanyName(String companyName)
	{
		clearAndType(eleCompanyName, companyName);
		return this;
	}
	
	public ViewLeadPage clickUpdate()
	{
		click(eleUpdate);
		return new ViewLeadPage();
	}
	
	
	
}
















