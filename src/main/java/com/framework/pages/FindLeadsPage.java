package com.framework.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class FindLeadsPage extends ProjectMethods{

	public FindLeadsPage() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how=How.XPATH,using="(//input[@name='companyName'])[2]") WebElement eleCompanyName;

	@FindBy(how=How.XPATH,using="//button[text()='Find Leads']") WebElement eleFindLeads;
	
	public FindLeadsPage enterCompanyName(String companyName)
	{
		clearAndType(eleCompanyName, companyName);
		return this;
		
	}
	
	public FindLeadsPage clickFindLeads()
	{
		click(eleFindLeads);
		return this;
	}
	@FindBy(how=How.XPATH,using="(//table[@class='x-grid3-row-table'])[1]") WebElement eleFindLeadsTable;
	//@FindBy(how=How.TAG_NAME,using="tr") WebElement eleFindLeadsTable;
	public ViewLeadPage clickFirstLeadId()
	{
		//in result table - select first matching record
		//WebElement eleTableName = locateElement("xpath", "(//table[@class='x-grid3-row-table'])[1]");
		  List<WebElement> rows = eleFindLeadsTable.findElements(By.tagName("tr"));
		 WebElement firstrow = rows.get(0);
	 List<WebElement> columns = firstrow.findElements(By.tagName("td"));
		 String firstMatchingLeadid=columns.get(0).getText();
		 
		 WebElement eleFirstMatchingLeadId = locateElement("linktext",firstMatchingLeadid );
		 click(eleFirstMatchingLeadId);
		return new ViewLeadPage();
	}
	
	
		
}













