package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC003_EditLead extends ProjectMethods {
	@BeforeTest
	public void setData()
	{
		testCaseName="TC003_EditLead";
		testDescription="Edit Lead";
		author="Nishanthi";
		category="Smoke";
		testNodes="Leads";
		dataSheetName="TC003";
		
	}
	
	
	
	@Test(dataProvider="fetchData")
	public void login(String username,String password,String companyName,String newCompanyName)
	{
		new LoginPage()
		.enterUsername(username)
		.enterPassword(password)
		.clickLogin().
		clickCrmSfa().
		clickLeads()
		.findLeads().
		enterCompanyName(companyName).
		clickFindLeads()
		.clickFirstLeadId()
		.clickEdit()
		.enterCompanyName(newCompanyName).clickUpdate();
	}
	

}
